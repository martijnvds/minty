from unittest import mock
from uuid import uuid4

from minty.cqrs import CommandBase, Event, EventService, QueryBase, event
from minty.entity import EntityBase
from minty.repository import RepositoryFactory


class DummyCommand(CommandBase):
    pass


class DummyQuery(QueryBase):
    pass


class DummyEntity(EntityBase):
    def entity_id(self):
        return "yolo"

    def __init__(self, event_service):
        self.event_service = event_service
        self.nothing = None
        self.some_value = 42
        self.some_string = "str"
        self.some_field = "some_value"

    @event("Dummied", extra_fields=["some_field"])
    def some_action(self, some_value):
        self.nothing = 3
        self.some_value = some_value
        self.some_string = "lalala"


class TestEventDecorator:
    def test_event_decorator(self):
        correlation_id = uuid4()
        user_uuid = uuid4()

        es = EventService(correlation_id, "d", "c", user_uuid)
        e = DummyEntity(es)

        e.some_action(666)

        assert e.some_value == 666
        assert len(es.event_list) == 1

        assert isinstance(es.event_list[0], Event)
        assert es.event_list[0].correlation_id == correlation_id
        assert es.event_list[0].domain == "d"
        assert es.event_list[0].context == "c"
        assert es.event_list[0].user_uuid == user_uuid
        assert es.event_list[0].entity_type == "DummyEntity"
        assert es.event_list[0].entity_id == e.entity_id
        assert es.event_list[0].event_name == "Dummied"
        assert es.event_list[0].changes == [
            {"key": "nothing", "new_value": 3, "old_value": None},
            {"key": "some_value", "new_value": 666, "old_value": 42},
            {"key": "some_string", "new_value": "lalala", "old_value": "str"},
        ]
        assert es.event_list[0].entity_data == {"some_field": "some_value"}


class TestCQRSBaseClasses:
    def test_command_base(self):
        mock_repo_factory = mock.MagicMock(spec=RepositoryFactory)
        mock_repo_factory.get_repository.return_value = "repo"
        event_service = EventService(uuid4(), "d", "c", uuid4())

        c = DummyCommand(
            repository_factory=mock_repo_factory,
            context="c",
            user_uuid=uuid4(),
            event_service=event_service,
        )
        r = c.get_repository("foo")

        assert r == "repo"
        mock_repo_factory.get_repository.assert_called_once_with(
            name="foo", context="c", event_service=event_service
        )

    def test_query_base(self):
        mock_repo_factory = mock.MagicMock(spec=RepositoryFactory)
        mock_repo_factory.get_repository.return_value = "repo"

        c = DummyQuery(
            repository_factory=mock_repo_factory,
            context="c",
            user_uuid=uuid4(),
        )
        r = c.get_repository("foo")

        assert r == "repo"
        mock_repo_factory.get_repository.assert_called_once_with(
            name="foo", context="c", event_service=None
        )

import io
import zipfile
from collections import namedtuple
from unittest import mock
from uuid import uuid4

import pytest
from swiftclient import Connection

from minty.exceptions import ConfigurationConflict
from minty.infrastructure.mime_utils import get_mime_type_from_handle, is_zip
from minty.infrastructure.openstack_swift import SwiftWrapper


class TestSwiftWrapper:
    def setup(self):
        config = {
            "container_name": "test",
            "segment_size": 107374182,
            "filestore_config": [
                {
                    "name": "store name",
                    "auth_version": "v3",
                    "auth": {
                        "username": "demo",
                        "password": "demo",
                        "auth_url": "http://127.0.0.1:35357/v3",
                        "user_domain_name": "Default",
                        "project_domain_name": "Default",
                        "project_name": "test",
                        "timeout": 60,
                    },
                }
            ],
        }
        swift_wrapper = SwiftWrapper(**config)
        self.swift_wrapper = swift_wrapper

    def test_initial_swift_wrapper_state(self):
        test = {
            "name": "store name",
            "auth_version": "v3",
            "auth": {
                "username": "demo",
                "password": "demo",
                "auth_url": "http://127.0.0.1:35357/v3",
                "user_domain_name": "Default",
                "project_domain_name": "Default",
                "project_name": "test",
                "timeout": 60,
            },
        }

        assert type(self.swift_wrapper.filestore_config) is list
        assert self.swift_wrapper.filestore_config[0] == test
        assert self.swift_wrapper.container_name == "test"
        assert type(self.swift_wrapper.segment_size) is int

    def _fake_put_object(self, container, obj, contents, response_dict):
        response_dict["headers"] = {
            "last-modified": "Wed, 26 Jun 2019 13:10:15 GMT",
            "content-length": "0",
            "etag": "56179b2937a6e956250ab491685a4efb",
            "content-type": "text/html; charset=UTF-8",
            "x-trans-id": "tx84ad44ef4de44981bf820-005d136eb6",
            "x-openstack-request-id": "tx84ad44ef4de44981bf820-005d136eb6",
            "date": "Wed, 26 Jun 2019 13:10:14 GMT",
        }

        return response_dict

    def test_is_zip(self):
        file_handle = io.BytesIO(b"PK\x03\x04")
        assert is_zip(file_handle)
        file_handle = io.BytesIO(b"fake")
        assert not is_zip(file_handle)

    @mock.patch("magic.detect_from_content", autospec=True)
    def test_get_mime_type_from_content(self, mock_detect_content):
        file_handle = io.BytesIO(b"PK\x03\x04")

        MimeType = namedtuple("MimeType", "mime_type")
        mock_detect_content.return_value = MimeType(mime_type="mock/content")

        assert get_mime_type_from_handle(file_handle) == "mock/content"

    @mock.patch("minty.infrastructure.mime_utils.get_mime_type_from_content")
    @mock.patch("minty.infrastructure.mime_utils.is_zip")
    @mock.patch("zipfile.ZipFile", autospec=True)
    def test_get_mime_type_from_handle(
        self, mock_zipfile, mock_is_zip, mock_get_from_content
    ):
        file_handle = io.BytesIO()

        zip_reader = mock.MagicMock()
        zip_reader.read = mock.MagicMock()
        zip_reader.read.return_value = """<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">
    <Default Extension="png" ContentType="image/png" />
    <Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml" />
    <Default Extension="xml" ContentType="application/xml" />
    <Override PartName="/docProps/app.xml" ContentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" />
    <Override PartName="/docProps/core.xml" ContentType="application/vnd.openxmlformats-package.core-properties+xml" />
    <Override PartName="/word/document.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml" />
    <Override PartName="/word/fontTable.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml" />
    <Override PartName="/word/footer1.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml" />
    <Override PartName="/word/footer2.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml" />
    <Override PartName="/word/footer3.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml" />
    <Override PartName="/word/header1.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.header+xml" />
    <Override PartName="/word/header2.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.header+xml" />
    <Override PartName="/word/header3.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.header+xml" />
    <Override PartName="/word/settings.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml" />
    <Override PartName="/word/styles.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml" />
    <Override PartName="/word/theme/theme1.xml" ContentType="application/vnd.openxmlformats-officedocument.theme+xml" />
    <Override PartName="/word/webSettings.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml" />
</Types>
        """
        mock_zipfile.return_value = zip_reader
        mock_is_zip.return_value = True

        assert (
            get_mime_type_from_handle(file_handle)
            == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        )

        zip_reader.read.return_value = ""
        mock_zipfile.return_value = zip_reader

        mock_is_zip.return_value = False
        mock_get_from_content.return_value = "mock/fobj"
        assert get_mime_type_from_handle(file_handle) == "mock/fobj"

        mock_is_zip.side_effect = zipfile.BadZipFile
        assert get_mime_type_from_handle(file_handle) == "mock/fobj"

    @mock.patch(
        "minty.infrastructure.openstack_swift.swiftwrapper.get_mime_type_from_handle"
    )
    @mock.patch(
        "minty.infrastructure.openstack_swift.SwiftWrapper._connect_to_swift"
    )
    def test_upload(
        self, mock_connect_to_swift, mock_get_mime_type_from_handle
    ):
        file_handle = mock.MagicMock()
        connection = mock.MagicMock()
        connection.put_object = self._fake_put_object
        mock_connect_to_swift.return_value = connection
        mock_get_mime_type_from_handle.return_value = "plain/text"

        res = self.swift_wrapper.upload(file_handle=file_handle, uuid=uuid4())
        assert type(res) is dict
        for key in ["uuid", "md5", "size", "mime_type", "storage_location"]:
            assert key in res

        res = self.swift_wrapper.upload(file_handle=file_handle, uuid=uuid4())

    def test_expextion_storage_name_key_upload(self):
        faulty_config = [
            {
                "auth_version": "v3",
                "auth": {
                    "username": "demo",
                    "password": "demo",
                    "auth_url": "http://127.0.0.1:35357/v3",
                    "user_domain_name": "Default",
                    "project_domain_name": "Default",
                    "project_name": "test",
                    "timeout": 60,
                },
            }
        ]
        file_handle = io.BytesIO(
            b"""some initial text data
            some initial text data
            some initial text data
            some initial text data"""
        )
        self.swift_wrapper.filestore_config = faulty_config
        with pytest.raises(ConfigurationConflict) as error:
            self.swift_wrapper.upload(file_handle=file_handle, uuid=uuid4())
            assert (
                error.value.message == "No name found for Swift configuration"
            )

    def test_auth_version_exception_connect_to_swift(self):
        faulty_config = {
            "no_auth_version_specified": {"name": "store name"},
            "unsupported_auth_version": {
                "name": "store name",
                "auth_version": "v5",
            },
        }
        for test_case, config in faulty_config.items():
            with pytest.raises(ConfigurationConflict) as error:
                self.swift_wrapper._connect_to_swift(file_store_config=config)
                assert (
                    error.value.message
                    == "No auth version specified or unsupported"
                )

    def test_noconfig_exception_on_upload(self):
        tempo_file = io.BytesIO(b"test")
        self.swift_wrapper.filestore_config = []

        with pytest.raises(ConfigurationConflict) as error:
            self.swift_wrapper.upload(file_handle=tempo_file, uuid=uuid4())
            assert (
                error.value.message
                == "No auth_version specified for Swift configuration"
            )
            assert tempo_file.closed is True

    def test_successful_connection_to_swift(self):
        proper_config = {
            "_connect_to_swift_v3": {
                "name": "store name",
                "auth_version": "v3",
                "auth": {
                    "username": "demo",
                    "password": "demo",
                    "auth_url": "http://127.0.0.1:35357/v3",
                    "user_domain_name": "Default",
                    "project_domain_name": "Default",
                    "project_name": "test",
                    "timeout": "60",
                },
            },
            "_connect_to_swift_v2": {
                "name": "store name",
                "auth_version": "v2",
                "auth": {
                    "username": "demo",
                    "password": "demo",
                    "auth_url": "http://127.0.0.1:35357/v2",
                    "tenant_name": "test",
                    "timeout": "60",
                },
            },
            "_connect_to_swift_legacy_auth": {
                "name": "store name",
                "auth_version": "v1",
                "auth": {
                    "username": "demo",
                    "password": "demo",
                    "auth_url": "http://127.0.0.1:35357/v1",
                    "tenant_name": "test",
                    "timeout": "60",
                },
            },
        }

        for test_name, test_case in proper_config.items():
            assert isinstance(
                self.swift_wrapper._connect_to_swift(test_case), Connection
            )

    def test_unsuccessful_connection_to_swift(self):
        wrong_config = {
            "_connect_to_swift_v3": {
                "name": "store name",
                "auth_version": "v3",
                "auth": {
                    "usename": "decvmo",
                    "pasword": "demaaaoa",
                    "auth_rl": "http://127.0.01:",
                    "project_domai_name": "Default",
                    "project_name": "test",
                    "timeout": 60,
                },
            },
            "_connect_to_swift_v2": {
                "name": "store name",
                "auth_version": "v2",
                "auth": {
                    "name": "sa.kdjfhsad.jhg",
                    "password": "aksjfhaskjhf",
                    "auth_url": "http://127.0..1:35357/v2",
                    "tenant_name": "test",
                    "timeout": 60,
                },
            },
        }

        for test_name, test_case in wrong_config.items():
            with pytest.raises(TypeError) as error:
                self.swift_wrapper._connect_to_swift(test_case)
                assert error.value.message == "Type error"

        wrong_config_timeout = {
            "_connect_to_swift_no_auth": {
                "name": "store name",
                "auth": {
                    "username": "useruser",
                    "password": "demo",
                    "auth_url": "http://127.0.1:35357/v1",
                    "ten_name": "test",
                    "timout": 60,
                },
            },
            "_connect_to_swift_unsupported_auth": {
                "name": "store name",
                "auth_version": "v8",
                "auth": {
                    "username": "useruser",
                    "password": "demo",
                    "auth_url": "http://127.0.1:35357/v1",
                    "ten_name": "test",
                    "timeout": "60",
                },
            },
        }

        for test_name, test_case in wrong_config_timeout.items():
            with pytest.raises(ConfigurationConflict) as error:
                self.swift_wrapper._connect_to_swift(test_case)
                assert error.value.message == "Key error"

        default_config_timeout = {
            "_connect_to_swift_legacy_auth": {
                "name": "store name",
                "auth_version": "v1",
                "auth": {
                    "username": "demo",
                    "password": "demo",
                    "auth_url": "http://127.0.0.1:35357/v1",
                    "tenant_name": "test",
                },
            }
        }

        for test_name, test_case in default_config_timeout.items():
            connection = self.swift_wrapper._connect_to_swift(test_case)
            assert connection.timeout == 60
            assert connection.auth_version == "1"

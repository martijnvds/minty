import io
from uuid import uuid4

import mock
import pytest

from minty.exceptions import ConfigurationConflict
from minty.infrastructure.s3 import S3Infrastructure, S3Wrapper


class TestS3Infrastructure:
    """Test the S3 infrastructure"""

    def setup(self):
        infra = S3Infrastructure()
        self.infra = infra
        self.full_config = {
            "filestore": {
                "type": "s3",
                "location_name": "S3Local",
                "bucket_name": "demo-bucket-002",
                "endpoint_url": "http://10.30.2.206:4572",
                "access_id": "test_id",
                "access_key": "test_key",
                "addressing_style": "path",
            }
        }

    def test_configuration_exception_s3_infra(self):
        tested_fields = [
            "location_name",
            "bucket_name",
            "endpoint_url",
            "access_id",
            "access_key",
        ]

        for tested_field in tested_fields:
            tested_config = dict(self.full_config)
            del tested_config["filestore"][tested_field]
            with pytest.raises(
                ConfigurationConflict,
                match="Invalid configuration for S3 found",
            ):
                self.infra(tested_config)

        del tested_config["filestore"]
        with pytest.raises(
            ConfigurationConflict, match="Invalid configuration for S3 found"
        ):
            self.infra(tested_config)

    @mock.patch("minty.infrastructure.mime_utils.get_mime_type_from_handle")
    @mock.patch("boto3.session.Session.client")
    def test_s3_upload(self, mock_client, mock_get_mime):
        mock_get_mime.return_value = "fake/mime"
        mock_put = mock.MagicMock()
        mock_put.put_object.return_value = {"ETag": '"some_md5"'}
        mock_client.return_value = mock_put

        wrapper = S3Wrapper([self.full_config["filestore"]])
        file_handle = io.BytesIO(b"test")

        file_uuid = uuid4()
        ret_value = wrapper.upload(file_handle, file_uuid)
        mock_get_mime.assert_called()

        assert ret_value["uuid"] == file_uuid
        assert ret_value["md5"] == "some_md5"
        assert ret_value["size"] == 4
        assert ret_value["mime_type"] == "fake/mime"
        assert (
            ret_value["storage_location"]
            == self.full_config["filestore"]["location_name"]
        )

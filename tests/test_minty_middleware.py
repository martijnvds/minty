import json
from unittest import mock
from uuid import uuid4

from minty.cqrs import EventService
from minty.middleware import AmqpPublisherMiddleware


class TestAMQPPublisherMiddleWare:
    """Test the AmqpPublisherMiddleware """

    def setup(self):
        self.exchange = "test_exchange"
        self.infrastructure_name = "amqp_test"
        self.publisher_name = "case_management"

        self.correlation_id = uuid4()
        self.domain = "test_domain"
        self.context = "test_context"
        self.user_uuid = uuid4()

        self.event_service = EventService(
            self.correlation_id, self.domain, self.context, self.user_uuid
        )

        publisher = AmqpPublisherMiddleware(
            publisher_name="case_management",
            infrastructure_name=self.infrastructure_name,
        )
        with mock.MagicMock() as mock_infra:
            mock_infra.get_config.return_value = {
                "amqp_test": {"publish_settings": {"exchange": self.exchange}}
            }
            self.mock_publisher = publisher(
                infrastructure_factory=mock_infra,
                correlation_id=self.correlation_id,
                domain=self.domain,
                context=self.context,
                user_uuid=self.user_uuid,
                event_service=self.event_service,
            )

    @mock.patch("minty.middleware.Message", spec=True)
    def test_amqp_publisher_middleware(self, mock_message):
        def mock_func():
            pass

        self.mock_publisher.infrastructure_factory.get_infrastructure.return_value = (
            "infra"
        )

        event_ids = [uuid4(), uuid4()]
        self.event_service.log_event(
            "FakeEntity",
            event_ids[0],
            "EventName",
            {"this": "that"},
            entity_data={},
        )
        self.event_service.log_event(
            "FakeEntity",
            event_ids[1],
            "EventName",
            {"this": "that"},
            entity_data={},
        )

        self.mock_publisher(func=mock_func)

        self.mock_publisher.infrastructure_factory.get_infrastructure.assert_called_with(
            infrastructure_name=self.infrastructure_name,
            context="test_context",
        )

        event_data = []
        for event in self.event_service.event_list:
            event_json = json.dumps(
                {
                    "id": str(event.uuid),
                    "created_date": event.created_date.isoformat(),
                    "correlation_id": str(self.correlation_id),
                    "context": event.context,
                    "domain": event.domain,
                    "user_uuid": str(event.user_uuid),
                    "entity_type": event.entity_type,
                    "entity_id": str(event.entity_id),
                    "event_name": event.event_name,
                    "changes": event.changes,
                    "entity_data": event.entity_data,
                },
                sort_keys=True,
            )
            event_data.append(
                mock.call(
                    channel="infra",
                    body=event_json,
                    properties={"content_type": "application/json"},
                )
            )
            event_data.append(
                mock.call().publish(
                    routing_key=f"zsnl.v2.test_domain.FakeEntity.EventName",
                    exchange=self.exchange,
                )
            )

        assert mock_message.create.mock_calls == event_data

import os

from minty.config.parser import ApacheConfigParser
from minty.config.store import FileStore


def test_filestore():
    parser = ApacheConfigParser()
    fs = FileStore(
        parser=parser,
        arguments={"directory": os.path.dirname(__file__) + "/data/test.d"},
    )

    assert fs.configuration == {
        "other-than-filename.com": {
            "aliases": "example.com,test.example.com",
            "test": "true",
            "other": "true",
        },
        "example.com": {
            "aliases": "example.com,test.example.com",
            "test": "true",
            "other": "true",
        },
        "test.example.com": {
            "aliases": "example.com,test.example.com",
            "test": "true",
            "other": "true",
        },
        "foobar.com": {"test": "true"},
        "third.example.com": {
            "aliases": "derde.example.com, 3.example.com",
            "test": "true",
            "other": "true",
        },
        "derde.example.com": {
            "aliases": "derde.example.com, 3.example.com",
            "test": "true",
            "other": "true",
        },
        "3.example.com": {
            "aliases": "derde.example.com, 3.example.com",
            "test": "true",
            "other": "true",
        },
    }

    config = fs.retrieve("test.example.com")
    assert config == {
        "aliases": "example.com,test.example.com",
        "other": "true",
        "test": "true",
    }

import os
from collections import namedtuple
from unittest import mock

import pytest
import statsd

from minty.config.parser import ApacheConfigParser, JSONConfigParser
from minty.config.store import FileStore
from minty.exceptions import ConfigurationConflict
from minty.infrastructure import InfrastructureFactory


class TestInfrastructureFactoryNone:
    """Test the Infrastructure Factory with a 'none' instance-config type."""

    def test_none_config(self):
        filename = os.path.dirname(__file__) + "/data/test-none.conf"
        factory = InfrastructureFactory(filename)
        assert factory.global_config["InstanceConfig"]["type"] == "none"
        assert factory.instance_config_store is None
        assert factory.statsd.connection.default_disabled

        class MockInfrastructure:
            def __init__(self, config):
                assert config["something"] == "tested"
                self.test = True

        factory.register_infrastructure(
            name="MockInfrastructure", infrastructure=MockInfrastructure
        )

        infra = factory.get_infrastructure(
            infrastructure_name="MockInfrastructure", context="foobar.com"
        )
        assert infra.test is True


class TestInfrastructureFactory:
    """Test the Infrastructure Factory."""

    def setup_class(self):
        filename = os.path.dirname(__file__) + "/data/test.json"
        factory = InfrastructureFactory(
            config_file=filename, config_parser=JSONConfigParser
        )

        assert factory.global_config["InstanceConfig"]["type"] == "file"
        assert (
            factory.global_config["InstanceConfig"]["arguments"]["directory"]
            == "tests/data/empty"
        )
        assert isinstance(factory.instance_config_store, FileStore)
        assert not factory.statsd.connection.default_disabled

        # Replace the connection with one that doesn't create network traffic
        # when running the test suite.
        statsd.Connection.set_defaults(disabled=True)
        factory.statsd.connection = statsd.Connection()

        config_dir = os.path.dirname(__file__) + "/data/test.d"
        factory.instance_config_store = FileStore(
            ApacheConfigParser(), {"directory": config_dir}
        )

        self.factory = factory

    def test_basic_usage(self):
        """Test infrastructure factory initialization."""

        class MockInfrastructure:
            def __init__(self, config):
                assert config["test"] == "true"
                self.test = True

        self.factory.register_infrastructure(
            name="MockInfrastructure", infrastructure=MockInfrastructure
        )

        assert "MockInfrastructure" in self.factory.registered_infrastructure

        infra = self.factory.get_infrastructure(
            infrastructure_name="MockInfrastructure", context="foobar.com"
        )

        assert infra.test

    def test_non_existent_infra(self):
        """Test failure condition."""
        with pytest.raises(ConfigurationConflict):
            self.factory.get_infrastructure(
                context="foobar.com", infrastructure_name="YYY"
            )

    def test_non_existent_context(self):
        """Test failure condition."""

        class MockInfrastructure:
            def __init__(self, config):
                pass

        self.factory.register_infrastructure(
            name="NonExistentContext", infrastructure=MockInfrastructure
        )

        with pytest.raises(ConfigurationConflict):
            self.factory.get_infrastructure(
                context="nonexistent.example.com",
                infrastructure_name="NonExistentContext",
            )

    @mock.patch(
        "minty.infrastructure.InfrastructureFactory.get_infra_from_infrastructure_factory"
    )
    def test_infra_with_context_None(self, mocked_method):

        InfraKey = namedtuple("InfraKey", ["context", "infrastructure_name"])

        self.factory.local_storage.infra = {
            InfraKey(
                context=None, infrastructure_name="infra_name"
            ): "infra_session"
        }

        mocked_method.return_value = "something_session"
        self.factory.get_infrastructure(
            context=None, infrastructure_name="something"
        )
        mocked_method.assert_called_with(
            context=None, infrastructure_name="something"
        )
        assert self.factory.local_storage.infra == {
            InfraKey(
                context=None, infrastructure_name="infra_name"
            ): "infra_session",
            InfraKey(
                context=None, infrastructure_name="something"
            ): "something_session",
        }

    def test_flush_local_storage(self):
        InfraKey = namedtuple("InfraKey", ["context", "infrastructure_name"])

        infra_with_clean_up = mock.MagicMock()

        self.factory.registered_infrastructure[
            "infra_with_clean_up"
        ] = infra_with_clean_up
        self.factory.registered_infrastructure[
            "infra_without_clean_up_method"
        ] = {}

        self.factory.local_storage.infra = {
            InfraKey(
                context="context", infrastructure_name="infra_with_clean_up"
            ): "infra_session",
            InfraKey(
                context="context2",
                infrastructure_name="infra_without_clean_up_method",
            ): "infra session",
        }
        self.factory.flush_local_storage()
        assert self.factory.local_storage.infra == {}
        infra_with_clean_up.clean_up.assert_called_with("infra_session")
